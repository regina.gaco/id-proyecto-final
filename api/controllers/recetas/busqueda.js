module.exports = {

  friendlyName: 'Busqueda',


  description: 'Realiza una búsqueda por categoría',


  extendedDescription:
`Esta acción intenta recuperar una receta de la base de datos de la categoría específicada. Si existen recetas
en esa categoría las regresa.`,
  inputs: { categoria: { description: 'La categoría a búscar, e.g. "vegetariano".', type: 'string',
      required: true
    },
  },


  exits: { success: { description: 'La receta ha sido encontrada.', extendedDescription:
`Al menos una receta fue recuperada de la base de datos coincidiendo con la categoría`
    },
  notfound: { description: `No se encontró ninguna receta que coicida con la categoría`,
      responseType: 'notFound'
    }
  },

  fn: async function busqueda({categoria}) {
    //Búsqueda por categoría, transformamos la categoría a minúsculas para evitar
    //errores
    var recetas = await Receta.find({ categoria: categoria.toLowerCase()}).
    exec(function (err, receta){
      if (err){
        return this.res(500, {error: 'Database error'});
      }
    });
  }
};
