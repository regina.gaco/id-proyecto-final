module.exports = {


  friendlyName: 'View Busqueda',


  description: 'Display "Busqueda" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/recetas/homepage',
    },

    redirect: {
      description: 'Page not found',
      responseType: 'redirect'
    }

  },


  fn: async function () {

    if (this.req.me) {
      throw {redirect: '/'};
    }

    return {};

  }


};
