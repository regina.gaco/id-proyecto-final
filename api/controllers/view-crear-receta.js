module.exports = {


  friendlyName: 'View crear receta',


  description: 'Display "Crear Receta" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/crear-receta',
    },
    redirect: {
      responseType: 'redirect',
      description: 'Requesting user is logged in, so redirect to the internal welcome page.'
    },
  },


  fn: async function () {

    if (this.req.me) {
      throw {redirect: '/'};
    }

    return {};

  }


};
