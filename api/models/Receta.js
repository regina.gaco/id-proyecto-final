/**
 * User.js
 *
 * A user who can log in to this application.
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    nombre: {
      type: 'string',
      required: true,
      maxLength: 200
    },

    pasos: {
      type: 'string',
      required: true
    },

    ingredientes: {
      type: 'string',
      required: true
    },

    /*
    imagen: {
      type: 'image'
    },*/

    //Vegetariano
    allergy1:{
     type: 'boolean'
	},
   //Diabetico
    allergy2:{
     type: 'boolean'
	},
  
    //Intolerante a la Lactosa
    allergy3:{
     type: 'boolean'
	},
    //No consume mariscos
    allergy4:{
     type: 'boolean'
  },
    //Alergia al maní
    allergy5:{
     type: 'boolean'
  },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝
    // n/a

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    // n/a

  },


};
